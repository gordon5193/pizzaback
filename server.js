const mysql = require('mysql');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const { json } = require('body-parser');

// parse application/json
app.use(bodyParser.json())
//cors policy
app.use(cors())

const conn = mysql.createConnection({
    host: "sql7.freemysqlhosting.net",
    user: "sql7359062",
    database: "sql7359062",
    password: "vQuVGlAzDu"
});

conn.connect((err) => {
    console.log('connecting')
    if (err) {
        throw err
    }
    console.log('connection established')
});


app.listen('5000', () => {
    console.log('server started')
})

const checkDuplicate = (username, phone, email) => {
    return new Promise((resolve, reject) => {
        const sql = `SELECT * FROM users WHERE username="${username}" 
        OR phone="${phone}" OR email="${email}"`
        conn.query(sql, (err, result) => {
            return err ? reject(err) : resolve(result);
        });
    });
}

const getUserId = (username) => {
    return new Promise((resolve, reject) => {
        const sql = `SELECT id FROM users WHERE username="${username}"`
        conn.query(sql, (err, result) => {
            return err ? reject(err) : resolve(result);
        });
    });
}

const registerUser = (userdata) => {
    return new Promise((resolve, reject) => {
        const sql = `INSERT INTO users (username, password, first_name, last_name, email, phone)
        VALUES("${userdata.username}", "${userdata.password}",
        "${userdata.frist}", "${userdata.last}",
        "${userdata.email}","${userdata.phone}")`;
        conn.query(sql, (err, result) => {
            return err ? reject(err) : resolve(result);
        });
    });
}

app.post('/register', async (req, res) => {
    const reg = req.body;
    const duplicate = await checkDuplicate(reg.username, reg.phone, reg.email);
    if (duplicate.length == 1) {
        res.send({ status: 'user exist', found: duplicate })
    }
    else {
        await registerUser(reg)
        res.send({ status: 'registered' })
    }
});

app.post('/login', (req, res) => {
    const log = req.body;
    console.log(log)
    const sql = `SELECT * FROM users WHERE username="${log.username}" AND password="${log.pass}"`
    conn.query(sql, (err, result) => {
        if (err) {
            throw err
        }
        if (result.length > 0) {
            res.send({ status: 'fine' })
        }
        else {
            res.send({ status: 'wrong' })
        }
    })
});

app.post('/getorders', async (req, res) => {
    const user = req.body.user;
    const getId = await getUserId(user);
    const id = getId[0].id
    const sql = `SELECT * FROM orders WHERE userid="${id}"`
    conn.query(sql, (err, result) => {
        if (err) {
            throw err
        }
        if (result.length > 0) {
            res.send({ orders: result })
        }
        else {
            res.send({ orders: [] })
        }
    })
})


app.post('/checkorder', async (req, res) => {
    const id = req.body.order;
    const sql = `SELECT * FROM orders WHERE id="${id}"`
    conn.query(sql, (err, result) => {
        if (err) {
            throw err
        }
        if (result.length > 0) {
            res.send({ orders: result })
        }
        else {
            res.send({ orders: [] })
        }
    })
})

app.post('/createorder', async (req, res) => {
    const order = req.body;
    let id = 0
    if (order.user) {
        id = await getUserId(order.user)
        id = id[0].id
    }
    const ship = order.ship.toString();
    const content = order.content.toString();
    console.log(ship)
    console.log(content)
    let sql = `INSERT INTO orders (userid, ship, content, times) VALUES(
    ${id}, '${ship}', '${content}', "${order.time}")`;
    let query = conn.query(sql, order, (err, result) => {
        if (err) {
            throw err
        }
        console.log(result)
        res.send({ status: result.insertId })
    })

})
